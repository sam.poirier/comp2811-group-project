/*
 *
 *    ______
 *   /_  __/___  ____ ___  ___  ____
 *    / / / __ \/ __ `__ \/ _ \/ __ \
 *   / / / /_/ / / / / / /  __/ /_/ /
 *  /_/  \____/_/ /_/ /_/\___/\____/
 *              video for sports enthusiasts...
 *
 *  2811 cw3 : twak
 */

#include <iostream>
#include <QApplication>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QMediaPlaylist>
#include <string>
#include <vector>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>
#include <QtCore/QFileInfo>
#include <QtWidgets/QFileIconProvider>
#include <QDesktopServices>
#include <QImageReader>
#include <QMessageBox>
#include <QCheckBox>
#include <QDate>
#include <QLabel>
#include <QSlider>
#include <QScrollArea>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include <QComboBox>
#include <QPainter>
#include <sstream>
#include "the_player.h"
#include "the_button.h"
#include <QFile>
#include <QDir>

#include "constants.h"

using namespace std;

static bool checkValidExtension(QString f) {
#if defined(_WIN32)
    return (f.contains(".wmv"));
#else
    return (f.contains(".mp4") || f.contains(".MOV"));
#endif
}

void drawText(QPainter *p, QImage *sprite, QUrl *url) {
    p->fillRect(QRect(0, sprite->rect().height() * 0.85, sprite->rect().width(),
                        sprite->rect().height()*0.15), Qt::gray);

    p->setPen(QPen(Qt::black));
    p->setFont(QFont("Times", sprite->rect().width()/15, QFont::Bold));
    p->drawText(QRect(sprite->rect().width() / 20, 0, sprite->rect().width(),
        sprite->rect().height()), Qt::AlignLeft | Qt::AlignBottom, url->fileName());
}

// read in videos and thumbnails to this directory
vector<TheButtonInfo> getInfoIn(string loc) {
    vector<TheButtonInfo> out = vector<TheButtonInfo>();
    QDir dir(QString::fromStdString(loc));
    QDirIterator it(dir);
    QMediaPlayer vid;
    while (it.hasNext()) { // for all files

        QString f = it.next();

        if (f.contains(".")) {
            if (checkValidExtension(f)) { // windows
                QString thumb = f.left(f.length() - 4) + ".png";
                if (!QFile(thumb).exists()) {
                    thumb = QString((string(ASSET_PATH) 
                        + string("/thumbnails/placeholder.png")).c_str());
                }

                QImageReader *imageReader = new QImageReader(thumb);
                QImage sprite = imageReader->read(); // read the thumbnail
                if (!sprite.isNull()) {
                    // convert the file location to a generic url
                    QUrl *url = new QUrl(QUrl::fromLocalFile(f));       

                    // do some drawing voodoo
                    // whodothevoodooyoudothevoodoo - now say that 10 times
                    QPainter p(&sprite);
                    drawText(&p, &sprite, url);

                    // voodoo to create an icon for the button
                    QIcon *ico = new QIcon(QPixmap::fromImage(sprite)); 
                    QEventLoop loop;
                    vid.setMedia(*url);
                    while (!vid.duration()) {
                        loop.processEvents();
                    }

                    out.push_back(TheButtonInfo(url, ico, QFileInfo(f).birthTime(),
                        QFileInfo(f).size(), vid.duration())); // add to the output list
                }
                else {
                    qDebug() << "warning: skipping video because I couldn't process thumbnail " 
                        << thumb << endl;
                }
            }
        }
        else { //assume names without "." are directories
            QString subfolder_path = dir.absoluteFilePath(f);
            qDebug() << subfolder_path << Qt::endl;
            vector<TheButtonInfo> subfolder; //not sure what happens when this goes out of scope
            subfolder = getInfoIn(subfolder_path.toStdString());
            // convert the file location to a generic url
            QUrl *url = new QUrl(QUrl::fromLocalFile(f)); 
            out.push_back(TheButtonInfo(url, subfolder));
        }
    }

    // sort by size (default)
    sort(out.begin(), out.end(), [](TheButtonInfo left, TheButtonInfo right) { 
        return left.metadataSize > right.metadataSize; 
    });
    return out;
}

static void init_internal_assets() {
    QDir asset_path(QString::fromStdString(ASSET_PATH)); //make all paths relative to asset path
    QString folder_image_path = 
        asset_path.absoluteFilePath(QString::fromStdString("thumbnails/Folder-icon.png"));
    QImageReader *imageReader = new QImageReader(folder_image_path);
    QImage sprite = imageReader->read(); // read the thumbnail
    TheButtonInfo::folder_icon = new QIcon(QPixmap::fromImage(sprite));
}

void createButtons(vector<TheButtonInfo> *videos, QWidget *buttonWidget, ThePlayer *player,  
    vector<TheButton*> *buttons,  QHBoxLayout *layout,  char *argv[]) {
    // create the buttons
    for (int i = 0; i < videos->size(); i++) {
        QVBoxLayout *btnVLayout = new QVBoxLayout();
        TheButton *button = new TheButton(buttonWidget);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo *)), 
            player, SLOT(jumpTo(TheButtonInfo *))); // when clicked, tell the player to play.
        buttons->push_back(button);
        btnVLayout->addWidget(button);
        button->init(&videos->at(i));

        QHBoxLayout *btnHLayout = new QHBoxLayout();

        QPushButton *saveBtn = new QPushButton();
        saveBtn->setText("Save");

        QObject::connect(saveBtn, &QPushButton::clicked, [&, button, argv]() {
            //Creates QDir Object called path that is used to mk a directory for the saved videos

            QDir path(argv[1]);
            if (!path.cd("save"))
                path.mkdir(SAVE_PATH);

            QUrl *fileUrl = button->info->url;
            QFile file(fileUrl->path()); //creation of a QFile Object used to move files
            file.copy(QString(argv[1]) + QDir::separator() + QString("save") 
                + QDir::separator() + fileUrl->fileName());
        });
        btnHLayout->addWidget(saveBtn);

        // add delete button
        QPushButton *deleteBtn = new QPushButton();
        deleteBtn->setText("Delete");
        // assign logic to remove video
        QObject::connect(deleteBtn, &QPushButton::clicked, 
            [&, player, button, btnVLayout, layout]() {
                QUrl *url = button->info->url;

                player->removeFromUrl(url, btnVLayout, layout);

                QMessageBox msgBox;
                msgBox.setText("Also delete on disk?");
                msgBox.setInformativeText(url->toLocalFile());
                msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
                msgBox.setDefaultButton(QMessageBox::No);
                int ret = msgBox.exec();

                if (ret == QMessageBox::Yes) {
                    QFile file(url->toLocalFile());
                    if (file.exists()) {
                        file.remove();
                    }
                }
            });
        btnHLayout->addWidget(deleteBtn);
        btnVLayout->addLayout(btnHLayout);
        layout->addLayout(btnVLayout);
    }
}

void checkVideosExist(vector<TheButtonInfo> *videos) {

    if (videos->size() == 0) {

        const int result = QMessageBox::question(
            NULL,
            QString("Tomeo"),
            QString("no videos found! download, unzip, and add command line argument to \"quoted\"") 
                + QString("file location. Download videos from Tom's OneDrive?"),
            QMessageBox::Yes |
                QMessageBox::No);

        switch (result) {
        case QMessageBox::Yes:
            QDesktopServices::openUrl(QUrl("https://bit.ly/2K44dmS"));
            break;
        default:
            break;
        }
        exit(-1);
    }
}

void setupScrollBar(ThePlayer *player, QSlider *progressBar, QLabel *progressText) {
    // video playing moves progress bar
    QObject::connect(player, &ThePlayer::positionChanged, [&, player, progressBar, progressText]() {
        int max = static_cast<int>(player->duration());
        int pos = static_cast<int>(player->position());
        QTime currentTime(0,0);
        QTime totalTime(0,0);
        currentTime = currentTime.addMSecs(pos);
        totalTime = totalTime.addMSecs(max);

        progressBar->setMaximum(max);
        progressBar->setValue(pos);

        QString format = "mm:ss";
        if (totalTime.hour() > 0) {
            format = "HH:" + format;
        }

        progressText->setText(currentTime.toString(format) + " / " + totalTime.toString(format));
    });
    // moving scrollbar moves video
    QObject::connect(progressBar, &QSlider::sliderMoved, [&, player, progressBar]() {
        qint64 newPos = progressBar->value();
        player->setPosition(newPos);
    });
}

void setupPlaybackSpeed(ThePlayer *player, QComboBox *playbackRateDropdown) {
    playbackRateDropdown->addItem("0.25x", 0.25f);
    playbackRateDropdown->addItem("0.50x", 0.50f);
    playbackRateDropdown->addItem("1x", 1.00f);
    playbackRateDropdown->addItem("1.5x", 1.50f);
    playbackRateDropdown->addItem("2x", 2.0f);
    playbackRateDropdown->setCurrentIndex(2);
    // need to specify which overloaded func to use, we want the int one
    void (QComboBox::*intCurrentIndexChanged)(int) = &QComboBox::currentIndexChanged;
    QObject::connect(playbackRateDropdown, intCurrentIndexChanged, [&](int idx) {
        float rate = playbackRateDropdown->itemData(idx).toFloat();
        player->setPlaybackRate(rate);
    });
}

void setupOptions(ThePlayer *player, QHBoxLayout *optionsLayout, QWidget *optionsWidget) {
    optionsLayout->setAlignment(Qt::AlignmentFlag::AlignLeft);

    QCheckBox *shuffleOption = new QCheckBox("shuffle");
    shuffleOption->connect(shuffleOption, SIGNAL(stateChanged(int)), 
        player, SLOT(setShuffleState(int)));

    QLabel *sortLabel = new QLabel("Sort by: ");
    sortLabel->setFixedSize(sortLabel->sizeHint());
    QComboBox *sortDropdown = new QComboBox();
    sortDropdown->addItem("Size");
    sortDropdown->addItem("Date");
    sortDropdown->addItem("Duration");
    sortDropdown->connect(sortDropdown, SIGNAL(currentTextChanged(QString)), 
        player, SLOT(setSorting(QString)));

    //add volume slider to the options layout
    QSlider* volumeSlider = new QSlider(Qt::Horizontal);
    volumeSlider->setMaximumWidth(200);
    QObject::connect(volumeSlider, SIGNAL(valueChanged(int)), player, SLOT(setVolume(int)));

    QLabel *playbackRateLabel = new QLabel("Speed: ");
    playbackRateLabel->setFixedSize(playbackRateLabel->sizeHint());
    QComboBox *playbackRateDropdown = new QComboBox();
    setupPlaybackSpeed(player, playbackRateDropdown);

    QPixmap *map = new QPixmap(QString((string(ASSET_PATH) 
        + string("/thumbnails/speaker.png")).c_str()));
    QLabel *volLabel = new QLabel;
    *map = map->scaledToWidth(20);
    volLabel->setPixmap(*map);
    volLabel->setMaximumWidth(20);

    optionsLayout->setAlignment(Qt::AlignLeft);
    optionsLayout->addWidget(sortLabel, Qt::AlignLeft);
    optionsLayout->addWidget(sortDropdown, Qt::AlignLeft);
    optionsLayout->addWidget(playbackRateLabel, Qt::AlignLeft);
    optionsLayout->addWidget(playbackRateDropdown, Qt::AlignLeft);
    optionsLayout->addWidget(shuffleOption, Qt::AlignLeft);
    optionsLayout->addStretch(4);
    optionsLayout->addWidget(volLabel, Qt::AlignRight);
    optionsLayout->addWidget(volumeSlider, Qt::AlignRight);
    optionsWidget->setLayout(optionsLayout);
}

int main(int argc, char *argv[]) {

    // let's just check that Qt is operational first
    qDebug() << "Qt version: " << QT_VERSION_STR << Qt::endl;

    // create the Qt Application
    QApplication app(argc, argv);

    //use voodoo to turn png assets into icons
    init_internal_assets();

    // collect all the videos in the folder
    vector<TheButtonInfo> videos;

    if (argc == 2) {
        videos = getInfoIn(string(argv[1]));
    }

    checkVideosExist(&videos);

    // the widget that will show the video
    QVideoWidget *videoWidget = new QVideoWidget;

    // the QMediaPlayer which controls the playback
    ThePlayer *player = new ThePlayer;
    player->setVideoOutput(videoWidget);
    player->setNotifyInterval(10);

    // a row of buttons
    QScrollArea *buttonWidgetContainer = new QScrollArea();
    QWidget *buttonWidget = new QWidget();
    // a list of the buttons
    vector<TheButton *> buttons;
    // the buttons are arranged horizontally
    QHBoxLayout *layout = new QHBoxLayout();
    buttonWidget->setLayout(layout);
    createButtons(&videos, buttonWidget, player, &buttons, layout, argv);
    buttonWidgetContainer->setWidget(buttonWidget);
    buttonWidgetContainer->setFixedHeight(180);

    // tell the player what buttons and videos are available
    player->setContent(&buttons, &videos);

    // create progress bar to skip through video
    QWidget *barWidget = new QWidget();
    QHBoxLayout *barLayout = new QHBoxLayout();
    QSlider *progressBar = new QSlider(Qt::Horizontal);
    QLabel *progressText = new QLabel();

    setupScrollBar(player, progressBar, progressText);

    //Create button to play or pause video
    QPushButton *playPauseButton = new QPushButton("play/pause");
    playPauseButton->connect(playPauseButton, SIGNAL( clicked()), player, SLOT( playPause() ) );
    barLayout->addWidget(playPauseButton);

    barLayout->addWidget(progressText);
    barLayout->addWidget(progressBar);
    barWidget->setLayout(barLayout);

    // create options controls
    QWidget *optionsWidget = new QWidget();
    QHBoxLayout *optionsLayout = new QHBoxLayout();
    setupOptions(player, optionsLayout, optionsWidget);

    // create the main window and layout
    QWidget window;
    QVBoxLayout *top = new QVBoxLayout();
    window.setLayout(top);
    window.setWindowTitle("tomeo");
    window.setMinimumSize(800, 680);

    // add the video and the buttons to the top level widget
    top->addWidget(videoWidget);
    top->addWidget(barWidget);
    top->addWidget(optionsWidget);
    top->addWidget(buttonWidgetContainer);

    // showtime!
    window.show();

    // wait for the app to terminate
    return app.exec();
}
