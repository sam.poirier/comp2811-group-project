//
// Created by twak on 11/11/2019.
//

#include "the_player.h"

using namespace std;

// all buttons have been setup, store pointers here
void ThePlayer::setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i) {
    buttons = b;
    infos = i;
    jumpTo(buttons -> at(0) -> info);
}

// change the image and video for one button every one second
void ThePlayer::shuffle() {
    if (!shuffleFlag) {
        return;
    }

    TheButtonInfo* i;
    // skip over folders
    while (true) {
        i = & infos -> at (rand() % infos->size() );
        if (i->isFolder() == false){
            break;
        }
        //otherwise keep looking
    }

    setMedia(*i->url);
}

void ThePlayer::playPause(){
    if (pauseFlag){ //If paused
        play(); // starting playing again...
        pauseFlag = false;
    }

    else{ // If playing
        pause();
        pauseFlag = true;
    }
}

void ThePlayer::playStateChanged (QMediaPlayer::State ms) {
    switch (ms) {
        case QMediaPlayer::State::StoppedState:
            play(); // starting playing again...
            break;
    default:
        break;
    }
}

void ThePlayer::removeFromUrl(QUrl * url, QVBoxLayout *subLayout, QHBoxLayout *pLayout) {
    for (int i = 0; i < infos->size(); i++) {
        if (*infos->at(i).url == *url){

            QLayoutItem* item;
            while ((item = subLayout->takeAt(0)) != NULL ) {
                delete item->widget();
                delete item;
            }
            pLayout->removeItem(subLayout);
            delete subLayout;

            return;
        }
    }
}

void ThePlayer::jumpTo (TheButtonInfo* button) {
    setMedia( * button -> url);
    play();
}

void ThePlayer::setShuffleState (int state) {
    this->shuffleFlag = (state == Qt::CheckState::Checked);
}

void ThePlayer::setSorting(QString type) {
    if(type == "Date") {
        sort(infos->begin(), infos->end(), 
            [](TheButtonInfo left, TheButtonInfo right) {
                return left.metadataDate > right.metadataDate; 
            });
        this->sortingType = Sort::Date;
    }
    else if(type == "Size") {
        sort(infos->begin(), infos->end(), 
            [](TheButtonInfo left, TheButtonInfo right) {
                return left.metadataSize > right.metadataSize; 
            });
        this->sortingType = Sort::Size;
    }
    else if(type == "Duration") {
        sort(infos->begin(), infos->end(), 
            [](TheButtonInfo left, TheButtonInfo right) {
                return left.metadataDuration > right.metadataDuration; 
            });
        this->sortingType = Sort::Duration;
    }

    for ( int i = 0; i < infos->size(); i++ ) {
        buttons->at(i)->init(&infos->at(i));
    }
}
