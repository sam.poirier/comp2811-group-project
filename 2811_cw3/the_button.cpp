//
// Created by twak on 11/11/2019.
//

#include "the_button.h"
#include <QIcon>

QIcon* TheButtonInfo::folder_icon = 0;

void TheButton::init(TheButtonInfo* i) {
    setIcon( *(i->icon) );
    info =  i;
}


void TheButton::clicked() {
    if(info->isFolder()){
        //change the QHBoxLayout somehow
    }
    else {
        emit jumpTo(info);
    }
}
