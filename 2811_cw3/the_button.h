//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_BUTTON_H
#define CW2_THE_BUTTON_H

#include <vector>
#include <QPushButton>
#include <QUrl>
#include <QDebug>
#include <QDate>



class TheButtonInfo {
private:
    bool is_folder;
public:
    static QIcon* folder_icon; // icon for all folders,

    QUrl* url; // video file to play (or folder to open)
    QIcon* icon; // icon to display

    QDateTime metadataDate;
    qint64 metadataSize;
    qint64 metadataDuration;

    std::vector<TheButtonInfo> folder_contents; // only used if button is a folder

    bool isFolder() { 
        return is_folder; 
    } // did somebody say encapsulation?

    TheButtonInfo(){ // default constructor for vector<TheButtonInfo>
        qDebug() << "BAD!! :( why are you using this default constructor?????" << Qt::endl;
    }

    //video button constructor
    TheButtonInfo ( QUrl* url, QIcon* icon, QDateTime metadataDate, qint64 metadataSize, 
        qint64 metadataDuration)  : is_folder(false), url(url), icon(icon), 
        metadataDate(metadataDate), metadataSize(metadataSize), metadataDuration(metadataDuration),
        folder_contents(0) {}

    // folder button constructor
    TheButtonInfo (QUrl* url, std::vector<TheButtonInfo> subfolder) : is_folder(true), url(url),
                                                                      icon(folder_icon),
                                                                      folder_contents(subfolder) {}
};

class TheButton : public QPushButton {
    Q_OBJECT

public:
    TheButtonInfo* info;

    TheButton(QWidget *parent) :  QPushButton(parent) {
         setIconSize(QSize(200,110));
         // if QPushButton clicked...then run clicked() below
         connect(this, SIGNAL(released()), this, SLOT (clicked() )); 
    }

    void init(TheButtonInfo* i);

private slots:
    void clicked();

signals:
    void jumpTo(TheButtonInfo*);

};

#endif //CW2_THE_BUTTON_H
