//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_PLAYER_H
#define CW2_THE_PLAYER_H


#include <QApplication>
#include <QMediaPlayer>
#include "the_button.h"
#include <vector>
#include <QTimer>
#include <QBoxLayout>

using namespace std;

enum Sort {
    Date,
    Size,
    Duration
};

class ThePlayer : public QMediaPlayer {

Q_OBJECT

private:
    QTimer* mTimer;
    long updateCount = 0;
    bool shuffleFlag = false;
    bool pauseFlag = false;
    Sort sortingType = Sort::Size;

public:
    ThePlayer() : QMediaPlayer(NULL) {
        setVolume(0); // be slightly less annoying
        connect (this, SIGNAL (stateChanged(QMediaPlayer::State)), 
            this, SLOT (playStateChanged(QMediaPlayer::State)) );

        mTimer = new QTimer(NULL);
        mTimer->setInterval(1000); // 1000ms is one second between ...
        mTimer->start();
        connect( mTimer, SIGNAL (timeout()), SLOT ( shuffle() ) ); // ...running shuffle method
    }



    // all buttons have been setup, store pointers here
    void setContent(vector<TheButton*>* b, vector<TheButtonInfo>* i);
    void removeFromUrl(QUrl *url, QVBoxLayout *subLayout, QHBoxLayout *pLayout);

    vector<TheButtonInfo>* infos;
    vector<TheButton*>* buttons;

private slots:

    // change the image and video for one button every one second
    void shuffle();

    // control play state
    void playPause();

    void playStateChanged (QMediaPlayer::State ms);

public slots:

    // start playing this ButtonInfo
    void jumpTo (TheButtonInfo* button);

    // set shuffle state
    void setShuffleState (int state);

    // set sorting type
    void setSorting(QString type);
};

#endif //CW2_THE_PLAYER_H
